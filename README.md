**WORK IN PROGRESS**

# Simple Phone

Un repo para mi proyecto de telefono DIY simple,
hecho con una pantallita de papel electrónico,
un chip SIM800L, y un microcontrolador.

La documentacion esta en el sitio de gitlab pages, generado por gitbuilding con `gitbuilding generate ci`, y publicada en:

* https://naikymen.gitlab.io/simple_phone/

![cablerio de componentes](media/cablerio.jpg  "cablerio")

----

# This project is documented with GitBuilding

## What is GitBuilding

GitBuilding is an OpenSource project for documenting hardware projects with minimal
effort, so you can stop writing and GitBuilding. GitBuilding is a python program that
works on Windows, Linux, and MacOS. More information on the GitBuilding project, or how
to install GitBuilding please see the [GitBuilding website](http://gitbuilding.io)
