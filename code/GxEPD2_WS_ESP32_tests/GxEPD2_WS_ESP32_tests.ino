# include "utiles.h"  // message strings to display
# include "funciones.h"  // message strings to display

#define ENABLE_GxEPD2_GFX 0
#include <GxEPD2_BW.h>
#include <GxEPD2_3C.h>
#include <Fonts/FreeMonoBold9pt7b.h>

#define GxEPD2_DISPLAY_CLASS GxEPD2_BW  // select the display class (only one), matching the kind of display panel
#define GxEPD2_DRIVER_CLASS GxEPD2_154_D67 // select the display driver class (only one) for your  panel // GDEH0154D67 200x200

// somehow there should be an easier way to do this
#define GxEPD2_BW_IS_GxEPD2_BW true
#define GxEPD2_3C_IS_GxEPD2_3C true
#define GxEPD2_7C_IS_GxEPD2_7C true
#define GxEPD2_1248_IS_GxEPD2_1248 true
#define IS_GxEPD(c, x) (c##x)
#define IS_GxEPD2_BW(x) IS_GxEPD(GxEPD2_BW_IS_, x)
#define IS_GxEPD2_3C(x) IS_GxEPD(GxEPD2_3C_IS_, x)
#define IS_GxEPD2_7C(x) IS_GxEPD(GxEPD2_7C_IS_, x)
#define IS_GxEPD2_1248(x) IS_GxEPD(GxEPD2_1248_IS_, x)

#if defined(ESP32)
#define MAX_DISPLAY_BUFFER_SIZE 65536ul // e.g.
#if IS_GxEPD2_BW(GxEPD2_DISPLAY_CLASS)
#define MAX_HEIGHT(EPD) (EPD::HEIGHT <= MAX_DISPLAY_BUFFER_SIZE / (EPD::WIDTH / 8) ? EPD::HEIGHT : MAX_DISPLAY_BUFFER_SIZE / (EPD::WIDTH / 8))
#elif IS_GxEPD2_3C(GxEPD2_DISPLAY_CLASS)
#define MAX_HEIGHT(EPD) (EPD::HEIGHT <= (MAX_DISPLAY_BUFFER_SIZE / 2) / (EPD::WIDTH / 8) ? EPD::HEIGHT : (MAX_DISPLAY_BUFFER_SIZE / 2) / (EPD::WIDTH / 8))
#elif IS_GxEPD2_7C(GxEPD2_DISPLAY_CLASS)
#define MAX_HEIGHT(EPD) (EPD::HEIGHT <= (MAX_DISPLAY_BUFFER_SIZE) / (EPD::WIDTH / 2) ? EPD::HEIGHT : (MAX_DISPLAY_BUFFER_SIZE) / (EPD::WIDTH / 2))
#endif
GxEPD2_DISPLAY_CLASS<GxEPD2_DRIVER_CLASS, MAX_HEIGHT(GxEPD2_DRIVER_CLASS)> display(GxEPD2_DRIVER_CLASS(/*CS=*/ 15, /*DC=*/ 27, /*RST=*/ 26, /*BUSY=*/ 25));
#endif

// comment out unused bitmaps to reduce code space used
#include "bitmaps/Bitmaps200x200.h" // 1.54" b/w

void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println("setup");
  
  display.init(115200); // uses standard SPI pins, e.g. SCK(18), MISO(19), MOSI(23), SS(5)
  SPI.end();            // release standard SPI pins, e.g. SCK(18), MISO(19), MOSI(23), SS(5)
  SPI.begin(13, 12, 14, 15); // map and init SPI pins SCK(13), MISO(12), MOSI(14), SS(15)

  // Setup del display
  display.setRotation(1);
  display.setFont(&FreeMonoBold9pt7b);
  //display.fillScreen(GxEPD_WHITE); // set the background to white (fill the buffer with value for white)
  //display.setTextColor(GxEPD_BLACK);  // on e-papers black text on white background is more pleasant to read

  // Update mode
  display.setFullWindow();  // full window mode is the initial mode, set it anyway
  // display.setPartialWindow(0, 0, display.width(), display.height());  // partial update mode

  // Texto para mostrar
  char string1[] = "holis :)";

  // Ubicaciones del cursor para alinear texto:
  // display.setCursor(x, y); // set the postition to start printing text
  //centrarCursor();
  centrarCursorTexto(string1);
  //cursor_a_cero();

  // Mostrar el texto:
  mostrarTexto(string1);

  delay(500);
}

void loop(){
  // Limpiar
  //blackScreen();
  clearScreen();
  
  // Set fullscreen partial update mode
  display.setPartialWindow(0, 0, display.width(), display.height());

  int16_t x, y; uint16_t bw, bh; // boundary box window
  
  // https://glenviewsoftware.com/projects/products/adafonteditor/adafruit-gfx-font-format/
  uint16_t line_height = FreeMonoBold9pt7b.yAdvance;
  
  int cx=0, cy=line_height;
  
  for (int r = 0; r < 4; r++){
    cursor_a_cero();

    // https://arduino.stackexchange.com/a/42987
    char texto[1];
    itoa(r, texto, 10);

    // it works for origin 0, 0, fortunately (negative y)
    display.getTextBounds(texto, cx, cy, &x, &y, &bw, &bh);

    Serial.println(cx); Serial.println(cy);
    Serial.println(x); Serial.println(bw);
    Serial.println(y); Serial.println(bh); 

    // Set partial update mode
    display.setPartialWindow(x,  y,
                             bw, bh);
   
    do {
    display.fillScreen(GxEPD_BLACK); // set the background to white (fill the buffer with value for white)
    } while (display.nextPage());

    do {
    display.fillScreen(GxEPD_WHITE); // set the background to white (fill the buffer with value for white)
    display.fillRect(x, y, bw, bh, GxEPD_BLACK);
    } while (display.nextPage());

    display.setCursor(cx, cy); // set the postition to start printing text
    mostrarTexto(texto);

    cx = x+bw;
    cy += line_height;
  }

  delay(1000);

  // Limpiar
  //clearScreen();
  
  // Apagar display
  //display.powerOff();
}
