    /////////////////////////////////////////////////////////////////
   //       ESP32 E-PAPER THERMOMETER       10/22/2020  v1.01     //
  //       Get the latest version of the code here:              //
 //        http://educ8s.tv/esp32-e-paper-thermometer           //
/////////////////////////////////////////////////////////////////

#include <GxEPD.h>
#include <GxGDEP015OC1/GxGDEP015OC1.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>

#include "BitmapGraphics.h"  // custom header file

#include <Fonts/FreeSansBold24pt7b.h>

GxIO_Class io(SPI, SS, 22, 21);
GxEPD_Class display(io, 16, 4);

int value = 0;

void setup() {
  
  Serial.begin(9600);
  display.init();
  
  display.drawExampleBitmap(gImage_splash, 0, 0, 200, 200, GxEPD_BLACK);
  display.update();
  delay(3000);

  display.drawExampleBitmap(gImage_gui, 0, 0, 200, 200, GxEPD_BLACK);
  
  display.update();

  display.drawExampleBitmap(gImage_gui, sizeof(gImage_gui), GxEPD::bm_default | GxEPD::bm_partial_update);
}

void loop() 
{
  value = value +1;
  
  showPartialUpdate(value);

  Serial.println(value);
  
  delay(2000);
}


void showPartialUpdate(float value)
{
  Serial.println("Updating display ...");
  String valueString = String(value,1);
  const char* name = "FreeSansBold24pt7b";
  const GFXfont* f = &FreeSansBold24pt7b;
  
  uint16_t box_x = 60;
  uint16_t box_y = 60;
  uint16_t box_w = 90;
  uint16_t box_h = 100;
  uint16_t cursor_y = box_y + 16;

  display.setRotation(45);
  display.setFont(f);
  display.setTextColor(GxEPD_BLACK);

  display.fillRect(box_x, box_y, box_w, box_h, GxEPD_WHITE);
  display.setCursor(box_x, cursor_y+38);
  display.print(valueString); 
  display.updateWindow(box_x, box_y, box_w, box_h, true);
}
