/**************************************************************************
 This is an example for our Monochrome OLEDs based on SSD1306 drivers
 **************************************************************************/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

/**************************************************************************
 * This sample code demonstrates the normal use of a TinyGPS object.
 * It requires the use of SoftwareSerial, and assumes that you have a
 * 4800-baud serial GPS device hooked up on pins 4(rx) and 3(tx).
 **************************************************************************/

#include <SoftwareSerial.h>
#include <TinyGPSPlus.h>

// The TinyGPSPlus object
TinyGPSPlus gps;

//#include <TinyGPS.h>
//TinyGPS gps;


int gpsTxPin = 27;
int gpsRxPin = 26;
SoftwareSerial ss(gpsTxPin, gpsRxPin);


void setup() {
  Serial.begin(9600);

  // GPS serial
  ss.begin(9600);

  Serial.println("Init.");

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
   
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  
  display.setCursor(0, 10);
  display.println("Hello GPS!");
  display.display(); 
//  delay(1000);
}

void loop() {

  // This sketch displays information every time a new sentence is correctly encoded.
  while (ss.available() > 0)
    gps.encode(ss.read());

  if (gps.altitude.isUpdated()){
    Serial.println(gps.altitude.meters());
  } else {
    Serial.println("No new data.");
  }
  
  // Debug: if we haven't seen lots of data in 5 seconds, something's wrong.
  if (millis() > 5000 && gps.charsProcessed() < 10) // uh oh
  {
    Serial.println("ERROR: not getting any GPS data!");
    // dump the stream to Serial
    Serial.println("GPS stream dump:");
    while (true) // infinite loop
      if (ss.available() > 0) // any data coming in?
        Serial.write(ss.read());
  }

  Serial.print("Sentences that failed checksum=");
  Serial.println(gps.failedChecksum());
   
  // Testing overflow in SoftwareSerial is sometimes useful too.
  Serial.print("Soft Serial device overflowed? ");
  Serial.println(ss.overflow() ? "YES!" : "No");

//  delay(1000);
}


void displayInfo()
{
  Serial.print(F("Location: ")); 
  if (gps.location.isValid())
  {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);

    
    display.clearDisplay();
    display.setCursor(0, 10);
    display.print(gps.location.lat());
    display.display(); 

//    display.clearDisplay();
    display.setCursor(1, 10);
    display.print(gps.location.lng());
    display.display(); 
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid())
  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.println();
}
