# GSM driver

## SIM800L

### Info, Pinout y Ejemplos

* [SIM800L guide](https://lastminuteengineers.com/sim800l-gsm-module-arduino-tutorial/) at Last Minute Engineers.
* [SIM800L_Hardware_Design_V1.00](https://lastminuteengineers.com/sim800l-gsm-module-arduino-tutorial/) (DRM may be removed with qpdf).

**MUY IMPORTANTE**

> You should be **very careful** to not to disconnect the GND before the VCC and always connect GND before VCC.
>
> Otherwise, the module can use the low voltage serial pins as ground and can get **destroyed instantly**.

[Pinout de lastminuteengineers](https://lastminuteengineers.com/sim800l-gsm-module-arduino-tutorial/#sim800l-gsm-module-pinout):

![sim800lpins](schematics/simm_800l/lastmineng-SIM800L-Pinout.png  "sim800lpins")

### Power

Estoy usando una pila de litio "MOTOMA" tipo LCR 26650, de 3.6V.

Está conectada al módulo directamente, agregando dos capacitores de 220uF en paralelo. Eso es porque tiene picos de consumo al conectarse a la red móvil, y ayudan a evitar la caida en el voltaje. Recomendaban uno de 1000 uF, pero alcanzó con eso que tenía a mano.

La pila se carga con un modulito ST6845-C v1.0, que además de cargar la pila tiene: protección de carga y bajo-voltaje, y un regulador de 5V con salida USB (que uso para alimentar el ESP32 y el GPS).

### Audio

Mandé el "speaker +/-" al line in de un scarlett solo para ver que onda, pero habia ruidos raros (clicks repetitivos, como cuando la llamada de un celu interfiere con parlantes de una compu).

Después lo conecte a un speaker bluettoth con el plug de 3.5mm y al llamarlo sonó un tono de llamada! Y al atender se escuchaba genial.

### Conexiones TX/RX

Obs: hay que usar un voltage divider con el arduino nano, que anda a 5V.

El ESP32 anda a 3.3v y se puede enchufar directo.

### AT commands

https://lastminuteengineers.com/sim800l-gsm-module-arduino-tutorial/

```
AT – It is the most basic AT command. It also initializes Auto-baud’er. If it works you should see the AT characters echo and then OK, telling you it’s OK and it’s understanding you correctly! You can then send some commands to query the module and get information about it such as

AT+CSQ – Check the ‘signal strength’ – the first # is dB strength, it should be higher than around 5. Higher is better. Of course it depends on your antenna and location!

AT+CCID – get the SIM card number – this tests that the SIM card is found OK and you can verify the number is written on the card.

AT+CREG? Check that you’re registered on the network. The second # should be 1 or 5. 1 indicates you are registered to home network and 5 indicates roaming network. Other than these two numbers indicate you are not registered to any network.

ATI – Get the module name and revision

AT+COPS? – Check that you’re connected to the network, in this case BSNL

AT+COPS=? – Return the list of operators present in the network.

AT+CBC – will return the lipo battery state. The second number is the % full (in this case its 93%) and the third number is the actual voltage in mV (in this case, 3.877 V)

ATA – Accepts incoming call.

ATH – Hangs up the call. On hanging up the call it sends NO CARRIER on the serial monitor indicating call couldn’t connect.
```

Serial monitor output useful stuff:

```
Module SIM800 R14.18
Firmware Revision:1418B04SIM800L24
SIM card number 8954318202063966898f
```

## Libraries Arduino / ESP

Hay varias libraries.

Me quedé con GSMsim.

Quizás valga la pena usar el serial directamente, y codear cosas de cero.

### Adafruit_FONA

* https://github.com/adafruit/Adafruit_FONA
  * Esta parece andar muy bien! El `FONAtest` ofrece un menú por el serial monitor para hacer cosas copadas.
  * No se me "registra" en la red por algun motivo.
  * Conexiones tal cual las de lastminuteengineers:

```
#define FONA_RX 2
#define FONA_TX 3
#define FONA_RST 4
```

### SIM800L

* https://github.com/cristiansteib/Sim800l
* https://cristiansteib.github.io/Sim800l

### SIM800L Library Revised

* https://www.arduino.cc/reference/en/libraries/sim800l-library-revised/
* Me tira error, no se por que.

### SIM800L HTTP Connector

* https://www.arduino.cc/reference/en/libraries/sim800l-http-connector/
* https://github.com/ostaquet/Arduino-SIM800L-driver
* Parece correr con estos settings, pero me da señal 0 y no se me registra en la network.

```
#define SIM800_RX_PIN 3
#define SIM800_TX_PIN 2
#define SIM800_RST_PIN 4
```

### Aspen SIM800

* Aspen SIM800: https://www.arduino.cc/reference/en/libraries/aspen-sim800/
* https://github.com/aspenforest/SIM800
* Solo se puede usar con los pines 10 y 11. Intente modificar la library pero parece que no tiene efecto :shrug:.
* Ver: https://github.com/aspenforest/SIM800/blob/master/src/SIM800.h

### GSMsim

* GSMsim: https://www.arduino.cc/reference/en/libraries/gsmsim/

Nota sobre `Serial1`: es similar al objeto `Serial` normal (que sería el `Serial0` si tuviera un numerito), pero refiere a la segunda interfaz serial del Arduino Mega (que tiene 4 en total). En cambio el Arduino UNO solo tiene una interfaz serial llamada `Serial` (a secas) y es la que manda y lee mensajes al serialm monitor por USB.

* Ver: https://forum.arduino.cc/t/arduino-uno-serial-begin-and-serial1-begin/56378/2

En vez de `Serial1`, usaría una instancia de `SoftwareSerial` o bien uno de los HardwareSerial del ESP32.

* Ver: https://circuits4you.com/2018/12/31/esp32-hardware-serial2-example/

Se recomienda usar hardware serial en el sitio de GSMsim.

Por ahora estoy usando software serial con GSMsim:

```
#include <GSMSimHTTP.h>

#define RXD1 14
#define TXD1 12
#define RESET_PIN 13 // you can use any pin.

#include <SoftwareSerial.h>
SoftwareSerial simSerial(RXD1, TXD1); //  ESP8226 12E: GPIO14 (D5) & GPIO12 (D6)
GSMSimHTTP http(simSerial, RESET_PIN);
```

En el setup hay que agregar esto:
```
  simSerial.begin(9600);
  
  while(!simSerial) {
    ; // wait for module for connect.
  }

  // Init module...
  http.init();
```

Lo más lejos que llegué con el ejemplo de HTTP por GPRS fue:

```
is Module Registered to Network?... 1
Signal Quality... 12
Operator Name... Claro AR
GPRS Init... Connect GPRS... 1
Check GPRS connection... 1
Get IP Address... 10.208.175.190
```

Finalmente probé un GET request a "www.google.com" y funcionó!

```
Get... METHOD:GET|HTTPCODE:280|LENGTH:15562
N⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮⸮|DATA:rset=UTF-8" http-equiv="Content-Type"><meta content="/logos/doodles/2021/claude-cahuns-127th-birthday-6753651837109117.2-law.gif" itemprop="image"><meta content="127⸮ aniversario del nacimiento de Claude Cahun" property="twitter:title"><meta content="127⸮ aniversario del nacimiento de Claude Cahun #GoogleDoodle" property="twitter:description"><meta content="127⸮ aniversario del nacimiento de Claude Cahun #GoogleDoodle" property="og:description"><meta content="summary_large_image" property="twitter:card"><meta content="@GoogleDoodles" property="twitter:site"><meta content="https://www.google.com/logos/doodles/2021/claude-cahuns-127th-birthday-6753651837109117.4-2xa.gif" property="twitter:image"><meta content="https://www.google.com/logos/doodles/2021/claude-cahuns-127th-birthday-6753651837109117.4-2xa.gif" property="og:image"><meta content="1000" property="og:image:width"><meta content="400" property="og:image:height"><meta content="https://www.google.com/logos/doodles/2021/claude-cahuns-127th-birthday-6753651837109117.4-2xa.gif" property="og:url"><meta content="video.other" property="og:type"><title>Google</title>

blablabla

</head>

blablabla

</body></html>
```

Pero el método de SSL no funcó:

```
Get with SSL and read returned data... ERROR:HTTP_ACTION_READ_ERROR
Close GPRS... 0
```

La segunda vez que lo intenté no funcionó, dijo:

```
Get... ERROR:HTTP_ACTION_READ_ERROR
Get with SSL and read returned data... ERROR:HTTP_ACTION_READ_ERROR
Close GPRS... 0
```

Así que no sé si la conexión es inestable o qué, pero algo fué algo :)

### Software Serial

* Simple `#include <SoftwareSerial.h>`.
* Ver: https://lastminuteengineers.com/sim800l-gsm-module-arduino-tutorial/

### TinyGSM

* Ver: https://www.instructables.com/ESP32-SIM800L-and-Barrier-Sensor/
* Repo: https://github.com/vshymanskyy/TinyGSM

## Otros antecedentes

Varios proyectos que usan SIM800L.

### Arduino

* https://www.instructables.com/Arduino-MobilePhone/
* https://cassiopeia.hk/arduinokidsphone/
* https://create.arduino.cc/projecthub/teachmesomething/arduino-gsm-sim-800l-relay-control-on-off-by-calling-2472b3
* https://create.arduino.cc/projecthub/SetNFix/smart-home-project-idea-with-sim-800l-module-c991b8
* https://descubrearduino.com/sim800l-gsm/
* https://www.teachmemicro.com/arduino-sim800l-tutorial/#The_FONA_Library

### ESP32

* https://www.instructables.com/ESP32-SIM800L-and-Barrier-Sensor/
* https://randomnerdtutorials.com/esp32-sim800l-publish-data-to-cloud/

