# Power Supply

## Lithium battery

`MOTOMA LL9K 3.6V LCR26650` lithium battery.

This battery is connected directly to the battery charger module, and to the SIM800L module (which unfortunately bypasses the undervoltage protection).

TO-DO: implement undervoltage protection for the GSM module.

## Charger module

`ST6845-C V1.0` battery charger module.

Fed by a generic 3000 mA 5V micro USB charger.

It's output feeds the `ESP32` microcontroller board, through its `Vin`/5V pin. The board shares this 5V source with the GPS module, and feeds 3.3V to the OLED display.

## Caps

Four `220 uF capacitors`, wired between VCC and GND of the SIM800L module (and closest to it).

If available, one 1000 uF cap would do.
