Stuff i read:

* https://lastminuteengineers.com/esp32-arduino-ide-tutorial/
* Very informative: https://www.studiopieters.nl/esp32-pinout/
  * https://github.com/AchimPieters/esp32-homekit-camera/blob/master/Images/ESP32-38Pin-devboard.pdf

Wiring:  https://uelectronics.com/wp-content/uploads/2019/10/AR1191-ESP32-38-Pin-Pinout.jpg

![AR1191-ESP32-38-Pin-Pinout](AR1191-ESP32-38-Pin-Pinout.jpg)
